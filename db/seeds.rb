# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Movie.delete_all
movies = Movie.create(
  [
    { 
      name: 'Inception', 
      position: 1,
    },
    { 
      name: 'The Matrix', 
      position: 2,
    },
    { 
      name: 'Mr. Nobody', 
      position: 3,
    },
    { 
      name: 'The Lord of the Rings: Fellowship of the Ring', 
      position: 4,
    },
    {
      name: 'Star Wars: A New Hope',
      position: 5,
    },
    {
      name: 'Les Misérables',
      position: 6,
    }
  ])

  movies[0].image.attach(io: URI.parse('https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_FMjpg_UX1000_.jpg').open, filename: 'inception.jpg');
  movies[1].image.attach(io: URI.parse('https://images-na.ssl-images-amazon.com/images/S/pv-target-images/06d86913c84a8e3c32f08eaabc56d8fded1c58943f1b4150c9fd0a9d4cea7a70._RI_V_TTW_.jpg').open, filename: 'matrix.jpg');
  movies[2].image.attach(io: URI.parse('https://images-na.ssl-images-amazon.com/images/S/pv-target-images/de6a57c14caa3cd4c168faee7ec0c8a895741bc2b5a5872bece8dfed29e01f33._RI_V_TTW_.jpg').open, filename: 'nobody.jpg');
  movies[3].image.attach(io: URI.parse('https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg').open, filename: 'tlotr.jpg');
  movies[4].image.attach(io: URI.parse('https://m.media-amazon.com/images/I/81aA7hEEykL.jpg').open, filename: 'starwarsiv.jpg');
  movies[5].image.attach(io: URI.parse('https://m.media-amazon.com/images/I/814OPpPD+uL._RI_.jpg').open, filename: 'lesmiserables.jpg');