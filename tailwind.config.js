/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: 'jit',
  purge: {
    enabled: ["production", "staging"].includes(process.env.NODE_ENV),
    content: [
      './app/views/**/*.html.erb',
      './app/views/**/*.html.haml',
      './app/helpers/**/*.rb',
      './app/javascript/**/*.js',
    ],
  },
  theme: {
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '4rem',
        xl: '5rem',
        '2xl': '6rem',
      },
    },
    extend: {
      colors: {
        'black-08': 'rgba(0, 0, 0, 0.8)',
        'black-06': 'rgba(0, 0, 0, 0.6)',
      },
      backgroundImage: {
        'search': "url('/img/search.svg')",
        'options': "url('/img/options.svg')",
        'pencil': "url('/img/pencil.svg')",
        'poster': "url('/img/movies/poster.jpg')",
        'movie-1': "url('/img/background-1.jpg')",
        'movie-2': "url('/img/background-2.jpg')",
      }
    },
  }
}
